package com.sensorviewera;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Handler;
import android.os.Message;

public class SensorListener implements SensorEventListener
{
    private Handler sender=null;
    private SensorData sensor_data=null;
    private Message msg=null;
    private boolean enable =true;
    public  SensorListener(SensorData sensor_data_,Handler sender_)
    {
        sender=sender_;
        sensor_data=sensor_data_;
    }
    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        if(!enable)return;
        byte[] tmp = sensor_data.getAccData(sensorEvent);
        if(tmp != null)
        {
            msg=Message.obtain();
            msg.what=EventType.NEW_DATA;
            msg.obj=tmp;
            sender.sendMessage(msg);
        }
        return;
    }
    public void setEnable(boolean e){enable =e;}
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}
}
