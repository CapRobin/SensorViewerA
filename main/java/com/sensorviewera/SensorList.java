package com.sensorviewera;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import java.util.ArrayList;
public class SensorList
{
    protected ExpandableListView list;
    protected CheckableExAdapter adapter;
    protected boolean enable_long_click;
    public boolean isCheckboxVisible=false;
    public SensorList(ExpandableListView l)
    {
        this.list=l;
        enable_long_click =false;

        setItemLongClicked();
        setChildClicked();
        setGroupClicked();
        ArrayList<GroupItem> groups=new ArrayList<GroupItem>();
        adapter=new CheckableExAdapter(l.getContext(),groups);
        list.setAdapter(adapter);
    }
    public SensorList(ExpandableListView l,ArrayList<GroupItem> groups)
    {
        this.list=l;
        enable_long_click =false;

        setItemLongClicked();
        setChildClicked();
        setGroupClicked();
        adapter=new CheckableExAdapter(l.getContext(),groups);
        list.setAdapter(adapter);
    }
    public void addGroup(GroupItem item)
    {
        adapter.addGroup(adapter.count(), item);
    }
    public void insertGroup(int idx,GroupItem item)
    {
        adapter.addGroup(idx, item);
    }
    public void removeGroup(GroupItem g)
    {
        adapter.removeGroup(g);
    }
    public GroupItem getGroup(int idx){return adapter.getGroupAt(idx);}
    public void refresh(){adapter.notifyDataSetChanged();}
    public int groupCount(){return adapter.count();}
    public void enableLongClick(boolean b){enable_long_click =b;}
    public void setAllChecked(boolean c)
    {
        adapter.setAllCBoxVisibility(c);
        isCheckboxVisible=c;
        adapter.setAllChecked(c);
        refresh();
    }

    protected void setItemLongClicked()
    {
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(!enable_long_click)
                    return true;
                if(view.getId()<0)
                    return true;
                collapseAll();
                adapter.getGroupAt(view.getId()).isChecked=true;
                adapter.setAllCBoxVisibility(true);
                refresh();
                isCheckboxVisible=true;
                return true;
            }
        });
    }
    protected void setChildClicked()
    {
        list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l)
            {
                SensorViewerAMainActivity a=(SensorViewerAMainActivity)list.getContext();
                a.listChildClickHandle(adapter.getGroupAt(i), ((ChildViewHolder) view.getTag()).item);
                return true;
            }
        });
    }
    protected void setGroupClicked()
    {
        list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
        {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l)
            {
                return false;
            }
        });
    }

    public ArrayList<GroupItem> getSelectedGroups()
    {
        if(isCheckboxVisible)
        {
            ArrayList<GroupItem> selected=new ArrayList<GroupItem>();
            int a_size=adapter.count();
            for(int i=0;i<a_size;i++)
                if(adapter.getGroupAt(i).isChecked)
                    selected.add(adapter.getGroupAt(i));
            return selected;
        }
        else
            return null;
    }
    public void collapseAll()
    {
        for(int i=0;i<adapter.count();i++)
            list.collapseGroup(i);
    }
}
